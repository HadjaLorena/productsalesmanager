package university.jala.productsalesmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductSalesManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductSalesManagerApplication.class, args);
    }

}
