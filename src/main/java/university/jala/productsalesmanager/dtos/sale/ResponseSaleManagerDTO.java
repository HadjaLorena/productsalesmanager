package university.jala.productsalesmanager.dtos.sale;

import java.time.LocalDateTime;

public record ResponseSaleManagerDTO(int productDTO, int quantity, float finalValue, LocalDateTime dateTime) {
}