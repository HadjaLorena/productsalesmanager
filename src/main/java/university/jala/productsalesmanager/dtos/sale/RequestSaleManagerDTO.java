package university.jala.productsalesmanager.dtos.sale;

public record RequestSaleManagerDTO(int productID, int quantity) {
}
