package university.jala.productsalesmanager.dtos.product;

public record ResponseProductManagerDTO(int productID, String name, float price, int quantity) {
}