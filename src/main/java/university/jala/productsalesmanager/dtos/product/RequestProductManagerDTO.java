package university.jala.productsalesmanager.dtos.product;

public record RequestProductManagerDTO(String name, float price, int quantity) {
}
