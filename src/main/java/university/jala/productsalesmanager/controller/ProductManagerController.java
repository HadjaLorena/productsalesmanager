package university.jala.productsalesmanager.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import university.jala.productsalesmanager.dtos.product.RequestProductManagerDTO;
import university.jala.productsalesmanager.dtos.product.ResponseProductManagerDTO;
import university.jala.productsalesmanager.services.ProductManagerServices;

import java.util.List;

@RestController
@RequestMapping("/produtos")
public class ProductManagerController {
    private final ProductManagerServices productManagerServices;

    public ProductManagerController(ProductManagerServices productManagerServices) {
        this.productManagerServices = productManagerServices;
    }

    @GetMapping
    public ResponseEntity<List<ResponseProductManagerDTO>> getAll(){
        return ResponseEntity.ok(productManagerServices.getAllProducts());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseProductManagerDTO> getProductByID(@PathVariable int id){
        return ResponseEntity.ok(productManagerServices.getProductByID(id));
    }

    @PostMapping
    public ResponseEntity<ResponseProductManagerDTO> postProduct(@RequestBody RequestProductManagerDTO requestProductManagerDTO){
        return ResponseEntity.status(HttpStatus.CREATED).body(productManagerServices.createProduct(requestProductManagerDTO));
    }
}