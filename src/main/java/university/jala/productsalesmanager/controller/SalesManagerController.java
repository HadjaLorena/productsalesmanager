package university.jala.productsalesmanager.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import university.jala.productsalesmanager.dtos.sale.RequestSaleManagerDTO;
import university.jala.productsalesmanager.dtos.sale.ResponseSaleManagerDTO;
import university.jala.productsalesmanager.services.SaleManagerServices;

import java.util.List;

@RestController
@RequestMapping("/vendas")
public class SalesManagerController {
    private final SaleManagerServices saleManagerServices;

    public SalesManagerController(SaleManagerServices saleManagerServices) {
        this.saleManagerServices = saleManagerServices;
    }

    @PostMapping
    public ResponseEntity<ResponseSaleManagerDTO> makeSale(@RequestBody RequestSaleManagerDTO requestSaleDTO){
        return ResponseEntity.status(HttpStatus.CREATED).body(saleManagerServices.makesASale(requestSaleDTO));
    }

    @GetMapping
    public ResponseEntity<List<ResponseSaleManagerDTO>> getAllSales(){
        return ResponseEntity.ok(saleManagerServices.getAllSales());
    }
}