package university.jala.productsalesmanager.services;

import org.springframework.stereotype.Service;
import university.jala.productsalesmanager.dtos.product.RequestProductManagerDTO;
import university.jala.productsalesmanager.dtos.product.ResponseProductManagerDTO;
import university.jala.productsalesmanager.exceptions.product.ProductInvalidNegativeValueExceptions;
import university.jala.productsalesmanager.exceptions.product.ProductNotFoundExceptions;
import university.jala.productsalesmanager.models.ProductModel;
import university.jala.productsalesmanager.repository.ProductManagerRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductManagerServices {
    private final ProductManagerRepository repository;

    //construtor
    public ProductManagerServices(ProductManagerRepository repository){
        this.repository = repository;
    }

    //lista todos os produtos cadastrados no banco de dados
    public List<ResponseProductManagerDTO> getAllProducts(){
        return genericReturnProductsList(repository.findAll());
    }

    //permite adicionar um novo produto ao banco de dados
    public ResponseProductManagerDTO createProduct(RequestProductManagerDTO requestProductManagerDTO){
        ProductModel product = new ProductModel(requestProductManagerDTO);

        if(requestProductManagerDTO.quantity() < 0 || requestProductManagerDTO.price() < 0)
            throw new ProductInvalidNegativeValueExceptions();

        repository.save(product);

        return new ResponseProductManagerDTO(product.getProductID(), product.getName(), product.getPrice(), product.getQuantity());
    }

    //busca e mostra os produtos presentes no banco de dados, por meio do ID
    public ResponseProductManagerDTO getProductByID(int productID){
        var product = repository.findById(productID).orElseThrow(ProductNotFoundExceptions::new);

        return new ResponseProductManagerDTO(product.getProductID(), product.getName(), product.getPrice(), product.getQuantity());
    }

    //retorna uma lista generica de produtos (converte list de produtos em list de response product)
    public List<ResponseProductManagerDTO> genericReturnProductsList(List<ProductModel> products){
        return  products
                .stream()
                .map(product -> new ResponseProductManagerDTO(product.getProductID(), product.getName(), product.getPrice(), product.getQuantity()))
                .collect(Collectors.toList());
    }
}