package university.jala.productsalesmanager.services;

import org.springframework.stereotype.Service;
import university.jala.productsalesmanager.dtos.sale.RequestSaleManagerDTO;
import university.jala.productsalesmanager.dtos.sale.ResponseSaleManagerDTO;
import university.jala.productsalesmanager.exceptions.product.ProductNotFoundExceptions;
import university.jala.productsalesmanager.exceptions.sale.SaleInsufficientProductsInStockExceptions;
import university.jala.productsalesmanager.exceptions.sale.SaleInvalidExceptions;
import university.jala.productsalesmanager.exceptions.sale.SaleQuantityLessThanOrEqualToZeroExceptions;
import university.jala.productsalesmanager.exceptions.sale.SaleThereAreNoProductsInStockExceptions;
import university.jala.productsalesmanager.models.SaleModel;
import university.jala.productsalesmanager.repository.ProductManagerRepository;
import university.jala.productsalesmanager.repository.SalesManagerRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SaleManagerServices {
    private final SalesManagerRepository salesManagerRepository;
    private final ProductManagerRepository productManagerRepository;

    public SaleManagerServices(ProductManagerRepository productManagerRepository, SalesManagerRepository salesManagerRepository) {
        this.productManagerRepository = productManagerRepository;
        this.salesManagerRepository = salesManagerRepository;
    }

    //realiza uma venda de um produto
    public ResponseSaleManagerDTO makesASale(RequestSaleManagerDTO requestSaleManagerDTO) {
        if (isSaleValid(requestSaleManagerDTO)) {
            var newSale = new SaleModel(requestSaleManagerDTO);

            newSale.setFinalValue(verifyTotalPrice(requestSaleManagerDTO));

            salesManagerRepository.save(newSale);

            updateQuantityProduct(requestSaleManagerDTO);

            return new ResponseSaleManagerDTO(newSale.getProductID(), newSale.getQuantity(), newSale.getFinalValue(), newSale.getDate());
        } else throw new SaleInvalidExceptions();
    }

    //lista todas as vendas realizadas
    public List<ResponseSaleManagerDTO> getAllSales() {
        var sales = salesManagerRepository.findAll();

        return sales
                .stream()
                .map(sale -> new ResponseSaleManagerDTO(sale.getProductID(), sale.getQuantity(), sale.getFinalValue(), sale.getDate()))
                .collect(Collectors.toList());
    }

    //verifica se uma venda é válida para ser realizada
    public boolean isSaleValid(RequestSaleManagerDTO requestSaleManagerDTO) {
        var product = productManagerRepository.findById(requestSaleManagerDTO.productID()).orElseThrow(ProductNotFoundExceptions::new);

        if (!(requestSaleManagerDTO.quantity() > 0)) throw new SaleQuantityLessThanOrEqualToZeroExceptions();
        if (!(product.getQuantity() > 0)) throw new SaleThereAreNoProductsInStockExceptions();
        if (!(requestSaleManagerDTO.quantity() <= product.getQuantity())) throw new SaleInsufficientProductsInStockExceptions();

        return true;
    }

    //realiza a atualizacao da quantidade de um produto
    public void updateQuantityProduct(RequestSaleManagerDTO requestSaleManagerDTO) {
        var product = productManagerRepository.findById(requestSaleManagerDTO.productID()).orElseThrow(ProductNotFoundExceptions::new);

        if (product.getQuantity() > 0) product.setQuantity(product.getQuantity() - requestSaleManagerDTO.quantity());

        productManagerRepository.save(product);
    }

    //verifica o valor final pago pelo usuário na compra, adicionando os discontos validos
    public float verifyTotalPrice(RequestSaleManagerDTO requestSaleManagerDTO) {

        var product = productManagerRepository.findById(requestSaleManagerDTO.productID()).orElseThrow(ProductNotFoundExceptions::new);

        float totalDiscount = 0;

        if(requestSaleManagerDTO.quantity() > 10)
            totalDiscount = 0.05f;
        if(requestSaleManagerDTO.quantity() > 20)
            totalDiscount = 0.10f;

        float totalPrice = requestSaleManagerDTO.quantity() * product.getPrice();

        return totalPrice - (totalPrice * totalDiscount);
    }
}