package university.jala.productsalesmanager.exceptions.product;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ProductManagerExceptions {
    @ExceptionHandler(ProductNotFoundExceptions.class)
    private ResponseEntity<String> productNotFoundExceptions(){
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ProductNotFoundExceptions().getMessage());
    }

    @ExceptionHandler(ProductInvalidNegativeValueExceptions.class)
    private ResponseEntity<String> productInvalidNegativeValueExceptions(){
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ProductInvalidNegativeValueExceptions().getMessage());
    }
}