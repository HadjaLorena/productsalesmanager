package university.jala.productsalesmanager.exceptions.product;

public class ProductNotFoundExceptions extends RuntimeException{
    public ProductNotFoundExceptions(){
        super("Produto não cadastrado no banco de dados.");
    }
}