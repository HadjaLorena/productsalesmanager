package university.jala.productsalesmanager.exceptions.product;

public class ProductInvalidNegativeValueExceptions extends RuntimeException{
    public ProductInvalidNegativeValueExceptions(){
        super("Valores negativos não são permitidos.");
    }
}
