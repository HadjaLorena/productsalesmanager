package university.jala.productsalesmanager.exceptions.sale;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class SaleManagerExceptions {
    @ExceptionHandler(SaleQuantityLessThanOrEqualToZeroExceptions.class)
    private ResponseEntity<String> saleQuantityLessThanOrEqualToZeroExceptions(){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new SaleQuantityLessThanOrEqualToZeroExceptions().getMessage());
    }

    @ExceptionHandler(SaleThereAreNoProductsInStockExceptions.class)
    private ResponseEntity<String> saleThereAreNoProductsInStockExceptions(){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new SaleThereAreNoProductsInStockExceptions().getMessage());
    }

    @ExceptionHandler(SaleInsufficientProductsInStockExceptions.class)
    private ResponseEntity<String> saleInsufficientProductsInStockExceptions(){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new SaleInsufficientProductsInStockExceptions().getMessage());
    }

    @ExceptionHandler(SaleInvalidExceptions.class)
    private ResponseEntity<String> saleInvalidExceptions(){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new SaleInvalidExceptions().getMessage());
    }
}