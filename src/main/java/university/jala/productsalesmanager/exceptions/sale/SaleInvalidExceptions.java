package university.jala.productsalesmanager.exceptions.sale;

public class SaleInvalidExceptions extends RuntimeException{
    public SaleInvalidExceptions(){
        super("Venda inválida.");
    }
}