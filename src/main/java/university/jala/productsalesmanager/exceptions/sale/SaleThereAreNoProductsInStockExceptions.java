package university.jala.productsalesmanager.exceptions.sale;

public class SaleThereAreNoProductsInStockExceptions extends RuntimeException{
    public SaleThereAreNoProductsInStockExceptions(){
        super("Não há produtos em estoque.");
    }
}