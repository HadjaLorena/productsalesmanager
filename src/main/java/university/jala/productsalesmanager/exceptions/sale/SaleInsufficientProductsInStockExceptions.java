package university.jala.productsalesmanager.exceptions.sale;

public class SaleInsufficientProductsInStockExceptions extends RuntimeException{
    public SaleInsufficientProductsInStockExceptions(){
        super("Produtos insuficientes em estoque.");
    }
}
