package university.jala.productsalesmanager.exceptions.sale;

public class SaleQuantityLessThanOrEqualToZeroExceptions extends RuntimeException{
    public SaleQuantityLessThanOrEqualToZeroExceptions(){
        super("A quantidade precisa ser maior que zero.");
    }
}