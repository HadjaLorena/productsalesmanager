package university.jala.productsalesmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import university.jala.productsalesmanager.models.SaleModel;

public interface SalesManagerRepository extends JpaRepository<SaleModel, Integer> {
}