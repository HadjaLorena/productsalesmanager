package university.jala.productsalesmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import university.jala.productsalesmanager.models.ProductModel;

public interface ProductManagerRepository extends JpaRepository<ProductModel, Integer> {
}