package university.jala.productsalesmanager.models;

import jakarta.persistence.*;
import lombok.*;
import org.apache.coyote.Request;
import university.jala.productsalesmanager.dtos.sale.RequestSaleManagerDTO;

import java.time.LocalDateTime;

@Entity
@Table(name = "sales")
@Getter
@Setter
@EqualsAndHashCode(of = "id_sale")
@NoArgsConstructor

public class SaleModel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)

    @Column(name = "id_sale")
    private int saleID;
    @Column(name = "id_product")
    private int productID;
    private int quantity;
    @Column(name = "final_value")
    private float finalValue;
    private LocalDateTime date;

    public SaleModel (RequestSaleManagerDTO requestSaleManagerDTO){
        this.productID = requestSaleManagerDTO.productID();
        this.quantity = requestSaleManagerDTO.quantity();
        this.date = LocalDateTime.now();
    }
}