package university.jala.productsalesmanager.models;

import jakarta.persistence.*;
import lombok.*;
import university.jala.productsalesmanager.dtos.product.RequestProductManagerDTO;

@Entity
@Table(name = "products")
@Getter
@Setter
@EqualsAndHashCode(of = "id_product")
@NoArgsConstructor

public class ProductModel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)

    @Column(name = "id_product")
    private int productID;
    private String name;
    private float price;
    private int quantity;

    public ProductModel(RequestProductManagerDTO requestProductManagerDTO){
        this.name = requestProductManagerDTO.name();
        this.price = requestProductManagerDTO.price();
        this.quantity = requestProductManagerDTO.quantity();
    }
}